package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import java.awt.Font;
import java.awt.Image;

import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import logica.Fabrica;
import logica.IUsuario;

import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class AltaUsuario extends JInternalFrame {
	private JTextField textNickUsuario;
	private JTextField textNombreUsuario;
	private JTextField textApellidoUsuario;
	private JTextField textEmailUsuario;
	private JTextField textNombreCanal;
	private ImageIcon imageIcon;
	private JTextField textImagenPath;
	private JTextArea textDescripcionCanal;
	
	Fabrica f;
	IUsuario interfaz;
	private JTextField textFechaUsuario;
	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public AltaUsuario() {
		setTitle("Alta Usuario");
		setBounds(50, 50, 683, 420);
		getContentPane().setLayout(null);
		
		textNickUsuario = new JTextField();
		textNickUsuario.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				String str = textNombreCanal.getText();
				if (str.isEmpty()){
					textNombreCanal.setText(textNickUsuario.getText());
				}
			}
		});
		textNickUsuario.setBounds(87, 212, 206, 20);
		getContentPane().add(textNickUsuario);
		textNickUsuario.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Nick:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 218, 67, 14);
		getContentPane().add(lblNewLabel);
		
		textNombreUsuario = new JTextField();
		textNombreUsuario.setBounds(87, 243, 206, 20);
		textNombreUsuario.setColumns(10);
		getContentPane().add(textNombreUsuario);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNombre.setBounds(10, 249, 67, 14);
		getContentPane().add(lblNombre);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(322, 11, 23, 368);
		getContentPane().add(separator);
		
		JLabel lblNewLabel_1 = new JLabel("Usuario");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(131, 11, 60, 14);
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblCanal = new JLabel("Canal");
		lblCanal.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCanal.setBounds(495, 11, 60, 14);
		getContentPane().add(lblCanal);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setHorizontalAlignment(SwingConstants.RIGHT);
		lblApellido.setBounds(0, 277, 77, 14);
		getContentPane().add(lblApellido);
		
		textApellidoUsuario = new JTextField();
		textApellidoUsuario.setColumns(10);
		textApellidoUsuario.setBounds(87, 271, 206, 20);
		getContentPane().add(textApellidoUsuario);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEmail.setBounds(10, 305, 67, 14);
		getContentPane().add(lblEmail);
		
		textEmailUsuario = new JTextField();
		textEmailUsuario.setColumns(10);
		textEmailUsuario.setBounds(87, 299, 206, 20);
		getContentPane().add(textEmailUsuario);
		
		JLabel lblFechaNacimiento = new JLabel("\r\nNacimiento:");
		lblFechaNacimiento.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFechaNacimiento.setBounds(10, 336, 67, 14);
		getContentPane().add(lblFechaNacimiento);
		
		JLabel lblImagen = new JLabel("Nombre:");
		lblImagen.setHorizontalAlignment(SwingConstants.RIGHT);
		lblImagen.setBounds(337, 39, 85, 14);
		getContentPane().add(lblImagen);
		
		textNombreCanal = new JTextField();
		textNombreCanal.setColumns(10);
		textNombreCanal.setBounds(432, 36, 206, 20);
		getContentPane().add(textNombreCanal);
		
		JLabel lblImagenPerfil = new JLabel("Imagen de Perfil");
		lblImagenPerfil.setHorizontalAlignment(SwingConstants.CENTER);
		lblImagenPerfil.setBounds(100, 47, 153, 127);
		//imageIcon = new ImageIcon(new ImageIcon(AltaUsuario.class.getResource("/presentacion/DefaultImage.jpg")).getImage().getScaledInstance(lblImagenPerfil.getHeight(), lblImagenPerfil.getWidth(), Image.SCALE_DEFAULT));
		imageIcon = new ImageIcon(AltaUsuario.class.getResource("/presentacion/DefaultImage.jpg"));
		Image img=imageIcon.getImage().getScaledInstance(lblImagenPerfil.getWidth(), lblImagenPerfil.getHeight(), Image.SCALE_DEFAULT);
		imageIcon= new ImageIcon(img);
		lblImagenPerfil.setIcon(imageIcon);
		
		getContentPane().add(lblImagenPerfil);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(101, 47, 1, 127);
		getContentPane().add(separator_2);
		
		JButton btnCargarImagen = new JButton("Cargar Imagen");
		btnCargarImagen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser aux = new JFileChooser();
				FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.JPG; *.PNG", "jpg", "png");
				aux.setFileFilter(filtro);
				int selecciona = aux.showOpenDialog(getContentPane());
				if (selecciona == JFileChooser.APPROVE_OPTION){
					File f = aux.getSelectedFile();
					textImagenPath.setText(f.getAbsolutePath());
					textImagenPath.setToolTipText(textImagenPath.getText());
					imageIcon = new ImageIcon(textImagenPath.getText());
					Image img=imageIcon.getImage().getScaledInstance(lblImagenPerfil.getWidth(), lblImagenPerfil.getHeight(), Image.SCALE_DEFAULT);
					imageIcon= new ImageIcon(img);
					lblImagenPerfil.setIcon(imageIcon);
				}
			}
		});
		btnCargarImagen.setBounds(219, 178, 23, 23);
		getContentPane().add(btnCargarImagen);
		
		textImagenPath = new JTextField();
		textImagenPath.setToolTipText("");
		textImagenPath.setBounds(101, 179, 119, 20);
		getContentPane().add(textImagenPath);
		textImagenPath.setColumns(10);
		
		JLabel lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDescripcion.setBounds(337, 76, 85, 14);
		getContentPane().add(lblDescripcion);
		
		JCheckBox chckbxPrivadoCanal = new JCheckBox("Privado");
		chckbxPrivadoCanal.setBounds(432, 209, 97, 23);
		getContentPane().add(chckbxPrivadoCanal);
		
		JComboBox comboCategoria = new JComboBox();
		comboCategoria.setBounds(432, 179, 206, 20);
		getContentPane().add(comboCategoria);
		f=Fabrica.getInstancia();
		interfaz=f.getIUsuario();
		String[] setString = interfaz.listarCategorias();
		comboCategoria.addItem("");
		if (setString!=null){
			for(int i=0; i<setString.length;i++){
				comboCategoria.addItem(setString[i]);
			}
		}
		comboCategoria.setSelectedIndex(0);
		
		
		JLabel lblCategoria = new JLabel("Categoria:");
		lblCategoria.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCategoria.setBounds(337, 182, 85, 14);
		getContentPane().add(lblCategoria);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(322, 271, 335, 2);
		getContentPane().add(separator_1);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				interfaz.ingresarDatosUsuario(textNickUsuario.getText(), "", textNickUsuario.getText(), textApellidoUsuario.getText(), textEmailUsuario.getText(), new Date(textFechaUsuario.getText()), textImagenPath.getText());
				interfaz.ingresarDatosCanal(textNombreCanal.getText(), 	textDescripcionCanal.getText(), chckbxPrivadoCanal.isSelected());
				interfaz.AltaUsuario();
				interfaz.Finalizar();
				JOptionPane.showMessageDialog(null, "Usuario Agregado!!", "Accion Correcta", JOptionPane.INFORMATION_MESSAGE);
				dispose();
			}
		});
		btnAceptar.setBounds(466, 356, 89, 23);
		getContentPane().add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(568, 356, 89, 23);
		getContentPane().add(btnCancelar);
		
		textDescripcionCanal = new JTextArea();
		textDescripcionCanal.setBounds(432, 67, 206, 98);
		getContentPane().add(textDescripcionCanal);
		
		textFechaUsuario = new JTextField();
		textFechaUsuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				
			}
		});
		textFechaUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		textFechaUsuario.setColumns(10);
		textFechaUsuario.setBounds(87, 333, 206, 20);
		getContentPane().add(textFechaUsuario);

	}
}
