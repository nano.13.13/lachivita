package presentacion;


import java.awt.Dialog.ModalExclusionType;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JDesktopPane;

public class Principal {

	private JFrame frmUytube;
  
	/**
	 * Launch the application.
	 */   
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() { 
				try {
					Principal window = new Principal(); 

					window.frmUytube.setVisible(true); 
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	} 
  
	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();
		
	}
  
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmUytube = new JFrame();
		frmUytube.setTitle("UyTube");
		frmUytube.setBounds(100, 100, 818, 593);
		frmUytube.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmUytube.getContentPane().setLayout(null);
		
		
		
		JMenuBar menuBar = new JMenuBar();
		frmUytube.setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mntmSalir.setHorizontalAlignment(SwingConstants.LEFT);
		mntmSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		JMenuItem mntmCargarDatos = new JMenuItem("Cargar Datos");
		mnArchivo.add(mntmCargarDatos);
		mnArchivo.add(mntmSalir);
		
		JMenu mnRegistrar = new JMenu("Registrar");
		menuBar.add(mnRegistrar);
		
		JMenuItem mntmAltaUsuario = new JMenuItem("Alta Usuario");
		mntmAltaUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame internalFrame = new AltaUsuario();
				//internalFrame.setBounds(155, 66, 226, 133);
				frmUytube.getContentPane().add(internalFrame);
				internalFrame.setVisible(true);
			}
		});
		mnRegistrar.add(mntmAltaUsuario);
		
		JMenuItem mntmAltaVideo = new JMenuItem("Alta Video");
		mnRegistrar.add(mntmAltaVideo);
		
		JMenuItem mntmAltaListaReproduccion = new JMenuItem("Alta Lista Reproduccion");
		mnRegistrar.add(mntmAltaListaReproduccion);
		
		JMenuItem mntmAltaCategoria = new JMenuItem("Alta Categoria");
		mnRegistrar.add(mntmAltaCategoria);
		
		JMenu mnModificar = new JMenu("Modificar");
		menuBar.add(mnModificar);
		
		JMenuItem mntmModificaUsuario = new JMenuItem("Modifica Usuario");
		mnModificar.add(mntmModificaUsuario);
		
		JMenuItem mntmModificarCanal = new JMenuItem("Modificar Canal");
		mnModificar.add(mntmModificarCanal);
		
		JMenuItem mntmModificarVideo = new JMenuItem("Modificar Video");
		mnModificar.add(mntmModificarVideo);
		
		JMenuItem mntmModificarListaReproduccion = new JMenuItem("Modificar Lista Reproduccion");
		mnModificar.add(mntmModificarListaReproduccion);
		
		JMenu mnConsultas = new JMenu("Consultas");
		menuBar.add(mnConsultas);
		
		JMenuItem mntmConsultaDeUsuario_1 = new JMenuItem("Consulta de Usuario");
		mnConsultas.add(mntmConsultaDeUsuario_1);
		
		JMenuItem mntmConsultaDeVideo = new JMenuItem("Consulta de Video");
		mnConsultas.add(mntmConsultaDeVideo);
		
		JMenuItem mntmConsultaListaReproduccion = new JMenuItem("Consulta Lista Reproduccion");
		mnConsultas.add(mntmConsultaListaReproduccion);
		
		JMenuItem mntmConsultaCategoria = new JMenuItem("Consulta Categoria");
		mnConsultas.add(mntmConsultaCategoria);
		
		JMenuItem mntmListaUsuariosExistentes = new JMenuItem("Lista Usuarios Existentes");
		mnConsultas.add(mntmListaUsuariosExistentes);
		
		JMenuItem mntmListaCategoriasExistentes = new JMenuItem("Lista Categorias Existentes");
		mnConsultas.add(mntmListaCategoriasExistentes);
		
		JMenu mnInteracciones = new JMenu("Interacciones");
		menuBar.add(mnInteracciones);
		
		JMenu mnUsuario = new JMenu("Usuario");
		mnInteracciones.add(mnUsuario);
		
		JMenuItem mntmSeguirUsuario = new JMenuItem("Seguir Usuario");
		mnUsuario.add(mntmSeguirUsuario);
		
		JMenuItem mntmDejarDeSeguir = new JMenuItem("Dejar de Seguir Usuario");
		mnUsuario.add(mntmDejarDeSeguir);
		
		JMenu mnVideo = new JMenu("Video");
		mnInteracciones.add(mnVideo);
		
		JMenuItem mntmComentarVideo = new JMenuItem("Comentar Video");
		mnVideo.add(mntmComentarVideo);
		
		JMenuItem mntmValorarVideo = new JMenuItem("Valorar Video");
		mnVideo.add(mntmValorarVideo);
		
		JMenu mnListaReproduccion = new JMenu("Lista Reproduccion");
		mnInteracciones.add(mnListaReproduccion);
		
		JMenuItem mntmAgregarVideo = new JMenuItem("Agregar Video");
		mntmAgregarVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JInternalFrame internalFrame = new AgregarVideoALista();
				//internalFrame.setBounds(155, 66, 226, 133);
				frmUytube.getContentPane().add(internalFrame);
				internalFrame.setVisible(true);
			}
		});
		mnListaReproduccion.add(mntmAgregarVideo);
		
		JMenuItem mntmQuitarVideo = new JMenuItem("Quitar Video");
		mntmQuitarVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame internalFrame = new QuitarVideoDeLista();
				//internalFrame.setBounds(155, 66, 226, 133);
				frmUytube.getContentPane().add(internalFrame);
				internalFrame.setVisible(true);
			}
		});
		mnListaReproduccion.add(mntmQuitarVideo);
	}
}
