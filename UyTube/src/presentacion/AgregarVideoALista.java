package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import logica.Fabrica;
import javax.swing.JComboBox;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import logica.DtListaReproduccion;
import logica.DtUsuario;
import logica.DtVideo;
import logica.IListaReproduccion;
import javax.swing.JSplitPane;
import java.awt.Panel;
import javax.swing.SpringLayout;
import java.awt.Font;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class AgregarVideoALista extends JInternalFrame {

	Fabrica f;
	IListaReproduccion interfaz;
	/**
	 * Launch the application.
	 */
	JComboBox<DtVideo> comboVideo;
	JComboBox<DtListaReproduccion> comboListaReproduccion;

	/**
	 * Create the frame.
	 */
	public AgregarVideoALista() {
		setTitle("Agregar Video a Lista");
		setBounds(0, 0, 565, 286);
		getContentPane().setLayout(null);
		
		Panel panel = new Panel();
		panel.setBounds(10, 10, 245, 170);
		getContentPane().add(panel);
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);
		
		JLabel lblSeleccionaUsuario = new JLabel("Seleccionar Usuario:");
		sl_panel.putConstraint(SpringLayout.WEST, lblSeleccionaUsuario, 10, SpringLayout.WEST, panel);
		panel.add(lblSeleccionaUsuario);
		
		JComboBox<DtUsuario> comboUsuariosSelVideo = new JComboBox();
		comboUsuariosSelVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JComboBox c = (JComboBox) arg0.getSource();
				DtUsuario dtUser = (DtUsuario)c.getSelectedItem();
				DtVideo[] listaVideo = interfaz.listarVideosDeUsuario(dtUser.getNick());
				comboVideo.removeAll();
				if (listaVideo!=null){
					for(int i=0; i<listaVideo.length; i++){
						comboVideo.addItem(listaVideo[i]);
					}
					comboVideo.setEnabled(true);
				}else{
					
					comboVideo.setEnabled(false);
				}
			}
		});
		sl_panel.putConstraint(SpringLayout.NORTH, comboUsuariosSelVideo, 6, SpringLayout.SOUTH, lblSeleccionaUsuario);
		sl_panel.putConstraint(SpringLayout.WEST, comboUsuariosSelVideo, 0, SpringLayout.WEST, lblSeleccionaUsuario);
		sl_panel.putConstraint(SpringLayout.EAST, comboUsuariosSelVideo, -10, SpringLayout.EAST, panel);
		panel.add(comboUsuariosSelVideo);
		
		JLabel lblSeleccionarVideo = new JLabel("Seleccionar Video:");
		sl_panel.putConstraint(SpringLayout.WEST, lblSeleccionarVideo, 0, SpringLayout.WEST, lblSeleccionaUsuario);
		panel.add(lblSeleccionarVideo);
		
		comboVideo = new JComboBox();
		comboVideo.setEnabled(false);
		sl_panel.putConstraint(SpringLayout.NORTH, comboVideo, 6, SpringLayout.SOUTH, lblSeleccionarVideo);
		sl_panel.putConstraint(SpringLayout.WEST, comboVideo, 0, SpringLayout.WEST, lblSeleccionaUsuario);
		sl_panel.putConstraint(SpringLayout.EAST, comboVideo, -10, SpringLayout.EAST, panel);
		panel.add(comboVideo);
		
		JLabel lblSeleccionVideo = new JLabel("Seleccion Video");
		sl_panel.putConstraint(SpringLayout.NORTH, lblSeleccionaUsuario, 19, SpringLayout.SOUTH, lblSeleccionVideo);
		sl_panel.putConstraint(SpringLayout.NORTH, lblSeleccionarVideo, 78, SpringLayout.SOUTH, lblSeleccionVideo);
		sl_panel.putConstraint(SpringLayout.NORTH, lblSeleccionVideo, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.EAST, lblSeleccionVideo, -63, SpringLayout.EAST, panel);
		lblSeleccionVideo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel.add(lblSeleccionVideo);
		
		Panel panel_1 = new Panel();
		panel_1.setBounds(292, 10, 245, 170);
		getContentPane().add(panel_1);
		SpringLayout sl_panel_1 = new SpringLayout();
		panel_1.setLayout(sl_panel_1);
		
		JLabel lblSeleccionListaReproduccion = new JLabel("Seleccion Lista Reproduccion");
		sl_panel_1.putConstraint(SpringLayout.NORTH, lblSeleccionListaReproduccion, 10, SpringLayout.NORTH, panel_1);
		sl_panel_1.putConstraint(SpringLayout.WEST, lblSeleccionListaReproduccion, 20, SpringLayout.WEST, panel_1);
		lblSeleccionListaReproduccion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_1.add(lblSeleccionListaReproduccion);
		
		JLabel lblSeleccionarUsuario = new JLabel("Seleccionar Usuario:");
		sl_panel_1.putConstraint(SpringLayout.WEST, lblSeleccionarUsuario, 10, SpringLayout.WEST, panel_1);
		panel_1.add(lblSeleccionarUsuario);
		
		comboListaReproduccion = new JComboBox();
		comboListaReproduccion.setEnabled(false);
		sl_panel_1.putConstraint(SpringLayout.NORTH, comboListaReproduccion, 129, SpringLayout.NORTH, panel_1);
		sl_panel_1.putConstraint(SpringLayout.WEST, comboListaReproduccion, 10, SpringLayout.WEST, panel_1);
		sl_panel_1.putConstraint(SpringLayout.EAST, comboListaReproduccion, -10, SpringLayout.EAST, panel_1);
		panel_1.add(comboListaReproduccion);
		
		JLabel lblSeleccionarListaDe = new JLabel("Seleccionar Lista de Reproduccion");
		sl_panel_1.putConstraint(SpringLayout.WEST, lblSeleccionarListaDe, 0, SpringLayout.WEST, lblSeleccionarUsuario);
		sl_panel_1.putConstraint(SpringLayout.SOUTH, lblSeleccionarListaDe, -6, SpringLayout.NORTH, comboListaReproduccion);
		panel_1.add(lblSeleccionarListaDe);
		
		JComboBox<DtUsuario> comboUsuarioSelLista = new JComboBox();
		comboUsuarioSelLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JComboBox c = (JComboBox) e.getSource();
				DtUsuario dtUser = (DtUsuario)c.getSelectedItem();
				DtListaReproduccion[] listaListaReproduccion = interfaz.listarListasDeUnUsuario(dtUser.getNick());
				comboListaReproduccion.removeAll();
				if (listaListaReproduccion!=null){
					for(int i=0; i<listaListaReproduccion.length; i++){
						
						comboListaReproduccion.addItem(listaListaReproduccion[i]);
						
					}
					comboListaReproduccion.setEnabled(true);
				}else{
					
					comboListaReproduccion.setEnabled(false);
				}
				
			}
		});
		sl_panel_1.putConstraint(SpringLayout.SOUTH, lblSeleccionarUsuario, -6, SpringLayout.NORTH, comboUsuarioSelLista);
		sl_panel_1.putConstraint(SpringLayout.EAST, comboUsuarioSelLista, -10, SpringLayout.EAST, panel_1);
		sl_panel_1.putConstraint(SpringLayout.NORTH, comboUsuarioSelLista, 70, SpringLayout.NORTH, panel_1);
		sl_panel_1.putConstraint(SpringLayout.WEST, comboUsuarioSelLista, 10, SpringLayout.WEST, panel_1);
		panel_1.add(comboUsuarioSelLista);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					if (comboListaReproduccion.getSelectedItem()!=null && comboVideo.getSelectedItem()!=null){
						interfaz.seleccionarLista(((DtListaReproduccion)comboListaReproduccion.getSelectedItem()).getNombre());
						interfaz.seleccionarVideo(((DtVideo)comboVideo.getSelectedItem()).getNombre());
						interfaz.agregarVideoALista();
						JOptionPane.showMessageDialog(null, "Video agregado a Lista Correctamente!", "Accion Correcta", JOptionPane.INFORMATION_MESSAGE);
						dispose();
					}else{
						JOptionPane.showMessageDialog(null, "Debe seleccionar una Lista de Reproduccion y un Video", "AlertBox - Exception", JOptionPane.INFORMATION_MESSAGE);
					}
				}catch(Exception ex){
					
					JOptionPane.showMessageDialog(null, ex.getMessage(), "AlertBox - Exception", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnAceptar.setBounds(349, 220, 89, 23);
		getContentPane().add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(448, 220, 89, 23);
		getContentPane().add(btnCancelar);
		
		f=Fabrica.getInstancia();
		interfaz = f.getIListaReproduccion();
		
		//Cargo datos de Usuario a Combo
		
		DtUsuario[] listUsuario=interfaz.listarUsuarios();
		if (listUsuario!=null){
			for(int i=0; i<listUsuario.length; i++){
				
				comboUsuariosSelVideo.addItem(listUsuario[i]);
				comboUsuarioSelLista.addItem(listUsuario[i]);
			}
		}
		
		
	}
}
