package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;

import logica.DtListaReproduccion;
import logica.DtUsuario;
import logica.DtVideo;
import logica.Fabrica;
import logica.IListaReproduccion;

import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class QuitarVideoDeLista extends JInternalFrame {

	Fabrica f;
	IListaReproduccion interfaz;
	JComboBox<DtListaReproduccion> comboLista;
	List listVideos;
	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public QuitarVideoDeLista() {
		setTitle("Quitar Video de Lista de reproduccion");
		setBounds(100, 100, 351, 349);
		getContentPane().setLayout(null);
		
		JComboBox<DtUsuario> comboUsuario = new JComboBox();
		comboUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox c = (JComboBox) e.getSource();
				DtUsuario dtUser = (DtUsuario)c.getSelectedItem();
				DtListaReproduccion[] listaListaReproduccion = interfaz.listarListasDeUnUsuario(dtUser.getNick());
				comboLista.removeAll();
				if (listaListaReproduccion!=null){
					for(int i=0; i<listaListaReproduccion.length; i++){
						
						comboLista.addItem(listaListaReproduccion[i]);
						
					}
					comboLista.setEnabled(true);
				}else{
					
					comboLista.setEnabled(false);
				}
			}
		});
		comboUsuario.setBounds(10, 39, 164, 20);
		getContentPane().add(comboUsuario);
		
		comboLista = new JComboBox();
		comboLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					JComboBox c = (JComboBox) e.getSource();
					DtListaReproduccion dtLista = (DtListaReproduccion)c.getSelectedItem();
					DtVideo[] listaVideos = interfaz.listarVideosDeLista(dtLista.getNombre());
					listVideos.removeAll();	
					if (listaVideos!=null){
						for(int i=0; i<listaVideos.length;i++){
							listVideos.add(listaVideos[i].getNombre());
						}
						listVideos.select(0);
					}
				}catch(Exception ex){
					JOptionPane.showMessageDialog(null, ex.getMessage(), "AlertBox - Exception", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		comboLista.setEnabled(false);
		comboLista.setBounds(10, 89, 164, 20);
		getContentPane().add(comboLista);
		
		listVideos = new List();
		listVideos.setBounds(10, 135, 315, 126);
		getContentPane().add(listVideos);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					String videoSelect = listVideos.getSelectedItem();
					if (videoSelect!=null){
						interfaz.seleccionarVideo(videoSelect);
						interfaz.eliminarVideoDeLista();
						listVideos.remove(listVideos.getSelectedItem());
						JOptionPane.showMessageDialog(null, "Video quitado de lista", "Accion Correcta", JOptionPane.INFORMATION_MESSAGE);
					}
				}catch(Exception ex){
					JOptionPane.showMessageDialog(null, ex.getMessage(), "AlertBox - Exception", JOptionPane.INFORMATION_MESSAGE);
				}
				
			}
		});
		btnAceptar.setBounds(144, 285, 89, 23);
		getContentPane().add(btnAceptar);
		
		JButton btnCancelar = new JButton("Salir");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(236, 285, 89, 23);
		getContentPane().add(btnCancelar);
		
		JLabel lblNewLabel = new JLabel("Seleccionar Usuario:");
		lblNewLabel.setBounds(10, 21, 104, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Seleccionar Lista:");
		lblNewLabel_1.setBounds(10, 70, 140, 14);
		getContentPane().add(lblNewLabel_1);
		
		f=Fabrica.getInstancia();
		interfaz= f.getIListaReproduccion();
		//Cargo datos de Usuario a Combo
		
		DtUsuario[] listUsuario=interfaz.listarUsuarios();
		if (listUsuario!=null){
			for(int i=0; i<listUsuario.length; i++){		
				comboUsuario.addItem(listUsuario[i]);
			}
		}

	}
}
