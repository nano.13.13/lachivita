package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SpringLayout;

import logica.Fabrica;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AltaCategoria extends JInternalFrame {
	private JTextField textField;

	Fabrica f;
	

	/**
	 * Create the frame.
	 */
	public AltaCategoria() {
		setTitle("Alta Categoria");
		setBounds(100, 100, 274, 122);
		SpringLayout springLayout = new SpringLayout();
		getContentPane().setLayout(springLayout);
		
		textField = new JTextField();
		springLayout.putConstraint(SpringLayout.SOUTH, textField, -50, SpringLayout.SOUTH, getContentPane());
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		getContentPane().add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnCancelar, 59, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, btnAceptar, 0, SpringLayout.NORTH, btnCancelar);
		springLayout.putConstraint(SpringLayout.EAST, btnAceptar, -6, SpringLayout.WEST, btnCancelar);
		springLayout.putConstraint(SpringLayout.EAST, textField, 0, SpringLayout.EAST, btnCancelar);
		springLayout.putConstraint(SpringLayout.WEST, btnCancelar, 173, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnCancelar, -10, SpringLayout.EAST, getContentPane());
		getContentPane().add(btnCancelar);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		springLayout.putConstraint(SpringLayout.WEST, textField, 9, SpringLayout.EAST, lblNewLabel_1);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_1, 25, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_1, 10, SpringLayout.WEST, getContentPane());
		getContentPane().add(lblNewLabel_1);

	}
}
