package logica;

public class CtrlListaReproduccion implements IListaReproduccion {

	static private CtrlListaReproduccion instancia = null;
	private Usuario usr1;
	private Usuario usr2;
	private Video video;
	private ListaReproduccion lista;

	
	static public CtrlListaReproduccion getInstancia(){
		
	    if (instancia == null)
	    {
	        instancia = new CtrlListaReproduccion();
	        instancia.usr1 = null;
	        instancia.usr2 = null;
	        instancia.video = null;
	        instancia.lista = null;
	    }

	    return instancia;
	}
	
	@Override
	public DtUsuario[] listarUsuarios() {
	
		ManejadorUsuario M_User = ManejadorUsuario.getInstance();		
		Usuario[] arrUsuarios = M_User.getUsuarios();
		if (arrUsuarios!=null){
			DtUsuario[] res = new DtUsuario[arrUsuarios.length];
			for(int i=0; i<arrUsuarios.length;i++){
				res[i] = arrUsuarios[i].getDtUsuario();
			}
			
			return res;
		}else{
			return null;
		}
	}

	@Override
	public DtVideo[] listarVideosDeUsuario(String nick) {
	
		ManejadorUsuario M_User = ManejadorUsuario.getInstance();
		this.usr1 = M_User.getUsuario(nick);
		Video[] arrVideos = usr1.listarVideos();
        DtVideo[] res = new DtVideo[arrVideos.length];
        
		for(int i=0; i<arrVideos.length;i++){
			res[i] = arrVideos[i].getDtVideo();
		}
		return res;
	}

	@Override
	public void seleccionarVideo(String nombreVideo) throws Exception {
		try{
			this.video = this.usr1.darVideo(nombreVideo);
		}catch(Exception e){
			throw e;
		}
	}

	@Override
	public DtListaReproduccion[] listarListasDeUnUsuario(String nick) {
	
		ManejadorUsuario M_User = ManejadorUsuario.getInstance();
		this.usr2 = M_User.getUsuario(nick);
		ListaReproduccion[] arrListas = usr2.listarListaDeReproduccion();
        DtListaReproduccion[] res = new DtListaReproduccion[arrListas.length];
        
		for(int i=0; i<arrListas.length;i++){
			res[i] = arrListas[i].getDtLista();
		}
		return res;
	}

	@Override
	public void seleccionarLista(String nombreLista) throws Exception {

		try{
			this.lista = this.usr1.darLista(nombreLista);
		}catch(Exception e){
			throw e;
		}
		
	}

	@Override
	public void agregarVideoALista() throws Exception {
		
		try{
			this.lista.agregarVideo(this.video);
		}catch(Exception e){
			throw e;
		}
		this.video = null;
		this.lista = null;
		this.usr1 = null;
		this.usr2 = null;

	}

	@Override
	public DtVideo[] listarVideosDeLista(String nombreLista) throws Exception {
	
		
		Video[] arrVideos;
		try {
			arrVideos = this.usr1.listarVideosDeUnaLista(nombreLista);
		} catch (Exception e) {
			throw e;
		}
        DtVideo[] res = new DtVideo[arrVideos.length];
        
		for(int i=0; i<arrVideos.length;i++){
			res[i] = arrVideos[i].getDtVideo();
		}
		return res;
	}

	@Override
	public void eliminarVideoDeLista() {
	
		this.lista.eliminarVideo(this.video);
		this.video = null;
		this.lista = null;
		this.usr1 = null;
		this.usr2 = null;
	}

	@Override
	public String[] listarCategorias() {
	
		ManejadorCategoria M_Cat = ManejadorCategoria.getInstance();
		return M_Cat.getCategorias();
	}

	@Override
	public void modificarLista(boolean privacidad) throws Exception {
		
		try {
			this.lista.setPrivado(privacidad);
		} catch (Exception e) {
			throw e;
		}
		this.video = null;
		this.lista = null;
		this.usr1 = null;
		this.usr2 = null;
	}
	
	@Override
	public boolean ingresarListaDefecto(String nombre) throws Exception{
		
		ManejadorListasDefecto M_List = ManejadorListasDefecto.getInstance();
		String[] nombres = M_List.getListasDefecto();
		boolean esta = false;
		for(int i=0; i<nombres.length;i++){
			if (nombre == nombres[i]){
				esta = true;
			}
		}
		
		if(!esta){
			M_List.aniadir(nombre);
			ManejadorUsuario M_User = ManejadorUsuario.getInstance();
			Usuario[] users = M_User.getUsuarios();
			for(int i=0; i<users.length;i++){				
				try{
					this.lista = new Defecto(nombre);
					users[i].getCanal().agregarListaReproduccion(this.lista);
				} catch (Exception e) {
					throw e;
				}
			}	
		}
		this.lista = null;
		this.usr1 = null;
		return !esta;
	} 
	
	@Override
	public boolean ingresarListaParticular(String nickUser, String nombreLista, boolean permiso) throws Exception{
		
		ManejadorUsuario M_User = ManejadorUsuario.getInstance();
		this.usr1 = M_User.getUsuario(nickUser);
		boolean agrego = false;
			try{
				this.lista = new Particular(nombreLista, permiso);
				this.usr1.getCanal().agregarListaReproduccion(this.lista);
				agrego = true;
			} catch (Exception e) {
				throw e;
			}
		this.lista = null;
		this.usr1 = null;
		return agrego;
	}
	

}
