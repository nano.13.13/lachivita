package logica;

import java.util.Set;
import java.util.HashSet;

public class ManejadorCategoria {

	private static ManejadorCategoria instancia=null;
	
	private Set<String> categorias;
	
	private ManejadorCategoria(){
		categorias=new HashSet<String>();
	}
	
	public static ManejadorCategoria getInstance(){
		
		if (instancia==null){
			instancia=new ManejadorCategoria();
		}
		return instancia;
	}
	
	public String[] getCategorias(){
		if (categorias.isEmpty()){
			return null;
		}else{
			Object[] objCat = categorias.toArray();
			String[] arrCat = new String[objCat.length];
			for(int i=0; i<objCat.length; i++){
				arrCat[i]=(String)objCat[i];
			}
			return arrCat;
		}
		
	}
	
	public void agregar(String s){
		
		if (!categorias.contains(s)){
			categorias.add(s);
		}
	}
	
	public boolean existeCategoria(String s){
		return (!categorias.contains(s));
	
	}
	
}
