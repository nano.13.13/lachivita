package logica;

public class Fabrica {

	    static private Fabrica instancia = null;
	    
        static public Fabrica getInstancia(){
        	
        	if (instancia == null)
    	    {
    	        instancia = new Fabrica();
    	    }

    	    return instancia;
        	
        }
	    
        public IListaReproduccion getIListaReproduccion(){
        	
        	return CtrlListaReproduccion.getInstancia();
        }
        public IUsuario getIUsuario(){
        	
        	return CtrlUsuario.getInstance();
        }
	
}
