package logica;
import java.util.Date;
import java.util.List;
import java.util.Iterator;

public class CtrlUsuario implements IUsuario {
	private String nick;
	private String apellido;
	private String nombre;
	private String correo;
	private Date fecha;
	private String img;
	private Usuario user;
	private static CtrlUsuario Instance = null;
	private String nomcan;
	private String des;
	private boolean privacidad;// 1 si es publico 0 si es privado
	private Canal channel;
	private String categoria;
	
	private CtrlUsuario(){
		this.nick="";
		this.apellido="";
		this.correo="";
		this.fecha=null;
		this.img="";
	}
	public static CtrlUsuario getInstance(){
		if (Instance==null){
			Instance = new CtrlUsuario();
		}
		return Instance;
	}
	
	/// CU ALTA USUARIO PRINCIPIO
		
	public void ingresarDatosUsuario(String nick,String categoria,String nombre,String apellido,String correo,Date fecha,String img){
		this.nick=nick;
		this.apellido=apellido;
		this.nombre=nombre;
		this.correo=correo;
		this.fecha=fecha;
		this.img=img;
		this.categoria=categoria;
	}
	public void ingresarDatosCanal(String nombre,String des,boolean privacidad){
		this.nomcan=nombre;
		this.des=des;
		this.privacidad=privacidad; 
	}
	public void AltaUsuario(){
		DtCanal can = new DtCanal(this.nomcan,this.des,this.categoria,this.privacidad);
		DtUsuario user = new DtUsuario(this.nick,this.nombre,this.apellido,this.correo,this.fecha,this.img);
		Usuario u = new Usuario(user,can);
		this.channel = u.getCanal();
	}
	
	public void CrearListas() throws Exception{
		ManejadorListasDefecto m = ManejadorListasDefecto.getInstance();
		String[] s = m.getListasDefecto();
		int i = s.length;
		for(int j=0;j<i;j++){
		ListaReproduccion p = new Defecto(s[j]);

			try{
				this.channel.agregarListaReproduccion(p);
				}
			catch(Exception e){
				throw e;
				}
		}
	}
	public void Finalizar(){
		this.channel=null;
		this.nick="";
		this.apellido="";
		this.nombre="";
		this.correo="";
		this.fecha.setDate(0);
		this.img="";
	}
	
	public String[] listarCategorias(){
		return null;
	}
	
	/// CU ALTA USUARIO FIN
	
	//  CU DEJAR DE SEGUIR A UN USUARIO PRINCIPIO
	
	public DtUsuario[] seleccionarUsuario(String nick1){
		ManejadorUsuario m = ManejadorUsuario.getInstance();
		Usuario u = m.getUsuario(nick1);
		this.user = u;
		List<Usuario> lista = u.getUsuariosSeguidos();
		Iterator<Usuario> it = lista.iterator();
		int i = 0;
		DtUsuario[] res = new DtUsuario[lista.size()];
		while(it.hasNext()){
			DtUsuario coso = new DtUsuario(it.next().getNick(),it.next().getNombre(),it.next().getApellido(),it.next().getEmail(),it.next().getNacimiento(),it.next().getImagen());	
			res[i] = coso;
			i++;
		}
		return res;
	}
	public void unfollow(String nick2){
		ManejadorUsuario m = ManejadorUsuario.getInstance();
		Usuario u = m.getUsuario(nick2);
		this.user.unfollow(u);
	}
	
	// CU DEJAR DE SEGUIR A UN USUARIO FIN
	
	// CU SEGUIR A UN USUARIO INICIO
	
	public DtUsuario seleccionarUsuario2(String nick1){
		ManejadorUsuario m = ManejadorUsuario.getInstance();
		Usuario u = m.getUsuario(nick1);
		this.user = u;
		DtUsuario p = u.getDtUsuario();
		return p;
	}
	public void seguir(String nick2){
		ManejadorUsuario m = ManejadorUsuario.getInstance();
		Usuario u = m.getUsuario(nick2);
		this.user.seguir(u);
	}
 	
	// CU SEGUIR A UN USUARIO FIN
	
	// CU CONSULTA USUARIO INICIO

	// la funcion que falta es seleccionarusuario2 que ya esta hecha en el caso de uso seguir a un usuario

	public DtUsuario[] listarUsuarios(){
	 ManejadorUsuario m = ManejadorUsuario.getInstance();
	 Usuario[] p = m.getUsuarios();
	 int tam = p.length;
	 DtUsuario[] res = new DtUsuario[tam];
	 for(int i=0;i<tam;i++){
		 Usuario u = p[i];
		 DtUsuario a = new DtUsuario(u.getNick(),u.getNombre(),u.getApellido(),u.getEmail(),u.getNacimiento(),u.getImagen());
		 res[i] = a;
	 }
	 return res;
	}
	public DtCanal seleccionarCanal(){
		Canal c = this.user.getCanal();
		this.channel = c;
		DtCanal res = new DtCanal(c.getNombre(),c.getDescripcion(),c.getCategoria(),c.isPrivado());
		return res;
	}
	public DtListaReproduccion[] listarListasDeReproduccion(){
		ListaReproduccion[] r = this.user.listarListaDeReproduccion();
		int tam = r.length;
		 DtListaReproduccion[] res = new DtListaReproduccion[tam];
		 for(int i=0;i<tam;i++){
			 ListaReproduccion aux = r[i];
			 DtListaReproduccion a = new DtListaReproduccion(aux.getNombre(),aux.isPrivado());
			 res[i] = a;
		 }
		 return res;
	}
	public DtVideo[] listarVideos(){
		Video[] v = this.channel.getVideos();
		int tam = v.length;
		 DtVideo[] res = new DtVideo[tam];
		 for(int i=0;i<tam;i++){
			 Video aux = v[i];
			 DtVideo a = new DtVideo(aux.getNombre(),aux.getDescripcion(),aux.getDuracion(),aux.getFechaPublicacion(),aux.getUrl(),aux.getCategoria(),aux.isPrivado());
			 res[i] = a;
		 }
		 return res;
	}
	public DtUsuario[] listarSeguidores(){
		List<Usuario> seguidores = this.user.darlistaseguidores();
		DtUsuario[] res = new DtUsuario[seguidores.size()];
		Iterator<Usuario> it = seguidores.iterator();
		int i=0;
		while(it.hasNext()){
			Usuario u = it.next();
			DtUsuario aux = new DtUsuario(u.getNick(),u.getNombre(),u.getApellido(),u.getEmail(),u.getNacimiento(),u.getImagen());
			res[i] = aux;
			i++;
		}
		return res;
	}
	public DtUsuario[] listarMisFollow(){
		List<Usuario> seguidos = this.user.getUsuariosSeguidos();
		Iterator<Usuario> it = seguidos.iterator();
		DtUsuario[] res = new DtUsuario[seguidos.size()];
		int i = 0;
		while(it.hasNext()){
			Usuario us = it.next(); 
			DtUsuario u = new DtUsuario(us.getNick(),us.getNombre(),us.getApellido(),us.getEmail(),us.getNacimiento(),us.getImagen());
			res[i]=u;
			i++;
		}
		return res;
	}
	public DtListaReproduccion seleccionarLista(String nombrelista){
		ListaReproduccion[] l = this.user.listarListaDeReproduccion();
		int it = l.length;
		int i = 0;
		DtListaReproduccion res = new DtListaReproduccion("",false);
		boolean bandera = false;
		while((i<it) && (!bandera)){
			if(	l[i].getNombre() == nombrelista){
				bandera = true;
				res = l[i].getDtLista();
			}
		}
		return res;
	}
	public DtVideo seleccionarVideo(String nombreVideo)throws Exception {
		Video v;	
		try{
		 v = this.user.darVideo(nombreVideo);
		}catch(Exception e){
			throw e;
		}
		DtVideo res = v.getDtVideo();
		return res;
	}	
	
	// CU CONSULTA USUARIO FIN
	
	//MODIFICAR DATOS DE USUARIO PRINCIPIO
	
	// la funcion que falta es seleccionarusuario2 que ya esta hecha en el caso de uso seguir a un usuario
	
	public String[] darListaUsuarios(){
		DtUsuario[] aux = listarUsuarios();
		int tam = aux.length;
		String[] res = new String[tam];
		for(int i=0;i<tam;i++){
			res[i]=aux[i].getNick();
		}
	return res;
	}
	public void modificarUsuario(DtUsuario datos){
		this.user.setImagen(datos.getImagen()); 
		this.user.setNacimiento(datos.getNacimiento());
		this.user.setApellido(datos.getApellido());
		//no nick y mail
	}
	
	//MODIFICAR DATOS DE USUARIO FIN
	
	//LISTAR USUARIOS EXISTENTES INICIO
	
	 // ESTE CASO DE USO SERIA UNA SOLA FUNCION QUE ES EXACTAMENTE darListasUsuarios del CU ANTERIOR
	
	//LISTAR USUARIOS EXISTENTES FIN
}	

