package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Usuario {
	private String nick;
	private String nombre;
	private String apellido;
	private String email;
	private Date nacimiento;
	private String imagen;
	private List<Usuario> usuariosSeguidos;
	private Canal canal;
	private List<Usuario> Seguidores;
	
	public Usuario(String nick, String nombre, String apellido, String email, Date nacimiento, String imagen){
		this.setNick(nick);
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setEmail(email);
		this.setNacimiento(nacimiento);
		this.setImagen(imagen);
		setUsuariosSeguidos(new ArrayList<Usuario>());
		this.setCanal(new Canal(this.nombre, "", "", true));
	}
	public Usuario(DtUsuario usr,DtCanal can){
		this.setNick(usr.getNick());
		this.setNombre(usr.getNombre());
		this.setApellido(usr.getApellido());
		this.setEmail(usr.getEmail());
		this.setNacimiento(usr.getNacimiento());
		this.setImagen(usr.getImagen());
		setUsuariosSeguidos(new ArrayList<Usuario>());
		this.setCanal(new Canal(can.getNombre(),can.getDescripcion(),can.getCategoria(),can.isPrivado()));
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public List<Usuario> getUsuariosSeguidos() {
		return usuariosSeguidos;
	}

	public void setUsuariosSeguidos(List<Usuario> seguidos) {
		this.usuariosSeguidos = seguidos;
	}
	
	public void seguirUsuario(Usuario u){
		usuariosSeguidos.add(u);
	}
	public void noSeguirUsuario(Usuario u){
		usuariosSeguidos.remove(u);
	}

	public Canal getCanal() {
		return canal;
	}

	public void setCanal(Canal canal) {
		this.canal = canal;
	}
	
	public DtUsuario getDtUsuario(){
		
		DtUsuario res = new DtUsuario();
		res.setNombre(this.getNombre());
		res.setApellido(this.getApellido());
		res.setNick(this.getNick());
		res.setEmail(this.getEmail());
		res.setImagen(this.getImagen());
		res.setNacimiento(this.getNacimiento());
		
		return res;
	}
	
	public Video[] listarVideos(){
		
		return this.canal.getVideos(); 
	}
	
	public Video darVideo(String nombreVideo) throws Exception{
		
		Video res;
		try{
			res = this.canal.obtenerVideo(nombreVideo);
		}catch(Exception e){
			throw e;
		}
		return res;
	}
	
	public ListaReproduccion[] listarListaDeReproduccion(){
		
		return this.canal.getListasReproduccion();
	}
	
	public ListaReproduccion darLista(String nombreLista) throws Exception{
		
		ListaReproduccion res;
		try{
			res = this.canal.obtenerListaReproduccion(nombreLista);
		}catch(Exception e){
			throw e;
		}
		return res;
	}
	
	public Video[] listarVideosDeUnaLista(String nombreLista) throws Exception{
		
		ListaReproduccion aux;
		try {
			aux = this.canal.obtenerListaReproduccion(nombreLista);
		} catch (Exception e) {
			throw e;
		}
		return aux.getVideos();
	}

	public void seguir(Usuario u){
		
		this.usuariosSeguidos.add(u);
	}
	
	public void unfollow(Usuario u) {
		
		int i = this.usuariosSeguidos.indexOf(u);
		this.usuariosSeguidos.remove(i);
	}
	public void seguidornuevo(Usuario u){
		this.Seguidores.add(u);
	}
	public void eliminarSeguidor(Usuario u){
		int i = this.Seguidores.indexOf(u);
		this.Seguidores.remove(i);
	}
	public List<Usuario> darlistaseguidores(){
		return this.Seguidores;
	}

	public void agregarVideo(Video vid) throws Exception{ 
		Canal c = this.getCanal();
		try {
			c.agregarVideo(vid);
		} catch (Exception e){
			throw e;
		}
	}
}	
