package logica;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ManejadorUsuario {
	 
	private Map<String, Usuario> usuarios;
	
	private static ManejadorUsuario instancia=null; 
	
	private ManejadorUsuario(){
		usuarios=new HashMap<String,Usuario>();
	}
	 
	public static ManejadorUsuario getInstance(){
		
		if (instancia==null){
			instancia=new ManejadorUsuario();
		}  
		return instancia;
	} 

	private boolean existeEmail(String email){
		Collection<Usuario> auxCol = usuarios.values();
		Object[] ar = auxCol.toArray();   
		int i=0;
		boolean res=false;
		while (i<ar.length && !res){
			res= res || (((Usuario)ar[i]).getEmail()==email);
		}
		 
		return res;
		 
	} 
	
	public void agregar(Usuario u) throws Exception{
		if (usuarios.containsKey(u.getNick())){
			throw new Exception("Ya existe un Usuario con el nick=" + u.getNick());
		}else if(this.existeEmail(u.getEmail())){
			throw new Exception("Ya existe un Usuario con el email=" + u.getEmail());
		}else{
			usuarios.put(u.getNick(), u);
		}
	}
	
	public Usuario[] getUsuarios(){
		if (usuarios.isEmpty()){
			return null;
		}else{ 
			Collection<Usuario> usrs = usuarios.values();
            Object[] o = usrs.toArray();
            Usuario[] arrUsuarios = new Usuario[o.length];
            
			for(int i=0; i<o.length;i++){
				arrUsuarios[i]=(Usuario)o[i];
			}
			return arrUsuarios;
		}
	}
	
	public Usuario getUsuario(String nickUsuario){
		if (usuarios.containsKey(nickUsuario)){
			return null;
		}else{
			return usuarios.get(nickUsuario);
		}
	}
	
	
}
