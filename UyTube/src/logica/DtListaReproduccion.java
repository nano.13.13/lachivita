package logica;


public class DtListaReproduccion {

	
	private String nombre;
	private Boolean privado;
	
	public DtListaReproduccion(){	}
	
	public DtListaReproduccion(String nombre, Boolean privado) {
		this.nombre = nombre;
		this.privado = privado;
	}
	 
	public String getNombre() {
		return nombre;
	}
	

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	public Boolean isPrivado() {
		return privado;
	}
	
	public void setPrivado(Boolean privado){
		this.privado = privado;
	}
	

}


/*
class DtDefecto extends DtListaReproduccion{

	public DtDefecto(String nombre) {
			super(nombre, true);
		
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void setPrivado(Boolean privado) throws Exception {
		if (!privado){
			throw new Exception("Las listas por defecto siempre son privadas.");
		}else{
			super.setPrivado(privado);
		}
	}
	
	
}

class DtParticular extends DtListaReproduccion{

	public DtParticular(String nombre, Boolean privado) {
		super(nombre, privado);
		// TODO Auto-generated constructor stub
	}
	
}
*/
