package logica;

public interface IListaReproduccion {
	
	// EN GENERAL
	
	public DtUsuario[] listarUsuarios();// LISTA TODOS LOS USUARIOS
	public DtVideo[] listarVideosDeUsuario(String nick); // LISTA VIDEOS DEL CANAL DE UN USUARIO
	public void seleccionarVideo(String nombreVideo) throws Exception; // RECUERDA EL VIDEO SELECCIONADO
	public DtListaReproduccion[] listarListasDeUnUsuario(String nick); // LISTA LAS L. DE REPRODUCCION DEL CANAL DE UN USUARIO
	public void seleccionarLista(String nombreLista) throws Exception; // RECUERDA LA LISTA SELECCIONADA
	
	//Agregar Video a Lista
	
	public void agregarVideoALista() throws Exception; // AGREGA EL VIDEO SELECCIONADO A LA LISTA SELECCIONADA
	
	// Quitar Video de Lista
	
	public DtVideo[] listarVideosDeLista(String nombreLista) throws Exception; // LISTA LOS VIDEOS DE UNA LISTA SELECCIONADA
	public void eliminarVideoDeLista(); // ELIMINA UN VIDEO SELECCIONADO DE UNA LISTA SELECCIONADA
	
	// Modificar Lista
	
	public String[] listarCategorias(); // LISTA TODAS LAS CATEGORIAS
	public void modificarLista(boolean privacidad) throws Exception; // MODIFICA LA PRIVACIDAD DE UNA LISTA (UNICO ATRIBUTO QUE PUEDE MODIFICARSE EN ESTA INSTANCIA)
	
	// Crear Lista ...
	
	//public void ingresarTipoLista(char tipo); NO ES NECESARIA, SE CONTROLA EN EL MAIN
	public boolean ingresarListaDefecto(String nombre) throws Exception; // RETORNA TRUE SI SE INGRES�
	public boolean ingresarListaParticular(String nickUser, String nombreLista, boolean permiso) throws Exception; // RETORNA TRUE SI SE INGRES�
	//public void modificarNombreLista(String nombreLista); NO ES NECESARIA, SE CONTROLA EN EL MAIN
	
	
	
	
}
