package logica;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Canal {
	private String nombre;
	private String descripcion;
	private String categoria;
	private boolean privado;
	private Map<String, Video> videos;
	private Map<String, ListaReproduccion> listasReproduccion;
	
	
	public Canal(String nombre, String descripcion, String categoria, boolean privado){
		this.setNombre(nombre);
		this.setDescripcion(descripcion);
		this.setCategoria(categoria);
		this.setPrivado(privado);
		this.setVideos(new HashMap<String,Video>());
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public boolean isPrivado() {
		return privado;
	}

	public void setPrivado(boolean privado) {
		this.privado = privado;
	}
	
	public Video[] getVideos() {
		if (videos.isEmpty()){
			return null;
		}else{
			Collection<Video> usrs = videos.values();
            Object[] o = usrs.toArray();
            Video[] arrVideos = new Video[o.length];
            
			for(int i=0; i<o.length;i++){
				arrVideos[i]=(Video)o[i];
			}
			return arrVideos;
		}
	}

	private void setVideos(Map<String, Video> videos) {
		this.videos = videos;
	}

	public ListaReproduccion[] getListasReproduccion() {
		
		if (listasReproduccion.isEmpty()){
			return null;
		}else{
			Collection<ListaReproduccion> usrs = listasReproduccion.values();
            Object[] o = usrs.toArray();
            ListaReproduccion[] arrListas = new ListaReproduccion[o.length];
            
			for(int i=0; i<o.length;i++){
				arrListas[i]=(ListaReproduccion)o[i];
			}
			return arrListas;
		}
	}

	public void setListasReproduccion(Map<String, ListaReproduccion> listasReproduccion) {
		this.listasReproduccion = listasReproduccion;
	}

	public void agregarVideo(Video v) throws Exception{
		
		if (videos.containsKey(v.getNombre())){
			throw new Exception("Ya existe un Video con el nombre = " + v.getNombre());
		}else{
			videos.put(v.getNombre(), v);
		}
		
	}
	
	public Video obtenerVideo(String nombre) throws Exception{
		if (videos.containsKey(nombre)){
			return videos.get(nombre);
		}else{
			throw new Exception("No existe video con nombre = " + nombre);
		}
	}
	
	public void agregarListaReproduccion(ListaReproduccion r) throws Exception{
		
		if (listasReproduccion.containsKey(r.getNombre())){
			throw new Exception("Ya existe una Lista de Reproduccion con el nombre = " + r.getNombre());
		}else{
			listasReproduccion.put(r.getNombre(), r);
		}
		
	}
	
	public ListaReproduccion obtenerListaReproduccion(String nombre) throws Exception{
		if (listasReproduccion.containsKey(nombre)){
			return listasReproduccion.get(nombre);
		}else{
			throw new Exception("No existe Lista de Reproduccion con el nombre = " + nombre);
		}
	}

}