package logica;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class Comentario {
	private Map<Integer, Comentario> respuestas;
	private int id;
	private Usuario user;
	private Date fechaCreacion; 
	private String texto;
	
	public Comentario(int id, Date fechaCreacion, String texto, Usuario user){
		this.setId(id);
		this.setFechaCreacion(fechaCreacion);
		this.setTexto(texto);
		this.setUser(user);
		setRespuestas(new TreeMap<Integer, Comentario>());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	} 
 
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getTexto() {
		return texto;
	} 

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Comentario[] getRespuestas() {
		if (respuestas.isEmpty()){
			return null;
		}else{
			Collection<Comentario> usrs = respuestas.values();
			Object[] o = usrs.toArray();
			Comentario[] arrComentarios = new Comentario[o.length];
        
			for(int i=0; i<o.length;i++){
				arrComentarios[i]=(Comentario)o[i];
			}
			return arrComentarios;
		}
	}

	public void setRespuestas(Map<Integer, Comentario> respuestas) {
		this.respuestas = respuestas;
	}
	
	public void agregarRespuesta(Comentario respuesta) {
		respuestas.put(respuesta.getId(), respuesta);
	}
	
	public Comentario obtenerRespuesta(int id){
		
		if(respuestas.containsKey(id)){
			return respuestas.get(id);
		}else{
			Comentario result=null;
			Collection<Comentario> c = respuestas.values();
			Object[] o = c.toArray();
			int i=0;
			while(i<o.length && result==null){
				result=((Comentario)o[i]).obtenerRespuesta(id);
				i++;
			}
			return result;
		}
		
	}
	
}
