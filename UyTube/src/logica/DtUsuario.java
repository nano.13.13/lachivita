package logica;

import java.util.Date;

public class DtUsuario {
	
	private String nick;
	private String nombre;
	private String apellido;
	private String email;
	private Date nacimiento;
	private String imagen;
	
	public DtUsuario(){	}
	
	public DtUsuario(String nick, String nombre, String apellido, String email, Date nacimiento, String imagen){
		this.setNick(nick);
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setEmail(email);
		this.setNacimiento(nacimiento);
		this.setImagen(imagen);
	
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}	
	
}
