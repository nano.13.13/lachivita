package logica;

import java.util.Date;
import java.util.Map;

public class CtrlVideo implements IVideo  {
	
	static private CtrlVideo instancia = null;

	private String nombreVideo;
	private int duracion;
	private String url;
	private String descripcion;
	private Date fechaPublicacion;
	private String categoria;
	private Usuario user;
	private Canal canal;
	private Video video;
	private Comentario comentario;
	
	static public CtrlVideo getInstancia() {
		
	    if (instancia == null) {
	        instancia = new CtrlVideo();
	    }

	    return instancia;
	}
	
	@Override
	public void altaVideo() {
		Video vid = new Video(nombreVideo, descripcion, duracion, fechaPublicacion, url, categoria, true);
		user.agregarVideo(vid);
	}
	
	public void seleccionarComentario(int id) throws Exception{
		try {
			comentario = video.obtenerComentario(id);
		}catch(Exception e) {
			throw e;
		} 
	}
	
	
	public void comentarVideo(String nick, Date fecha, String texto, int id) {
		ManejadorUsuario M_User = ManejadorUsuario.getInstance();
		Usuario user = M_User.getUsuario(nick);
		
		Comentario nuevoComentario = new Comentario(id, fecha, texto, user);
		if (comentario != null) {
			comentario.agregarRespuesta(comentario);
		} else {
			video.agregarComentario(comentario);
		}
	}
	
	public DtUsuario[] listarUsuarios() {
		ManejadorUsuario M_User = ManejadorUsuario.getInstance();		
		Usuario[] arrUsuarios = M_User.getUsuarios();
		if (arrUsuarios!=null){
			DtUsuario[] res = new DtUsuario[arrUsuarios.length];
			for(int i=0; i<arrUsuarios.length;i++){
				res[i] = arrUsuarios[i].getDtUsuario();
			}
			
			return res;
		}else{
			return null;
		}
	}
	
	public String[] darListaVideos(String nick) {
		ManejadorUsuario M_User = ManejadorUsuario.getInstance();		
		user = M_User.getUsuario(nick);
		canal = user.getCanal();
		Video[] videos = canal.getVideos();
		String[] vid = new String[videos.length];
		
		if (videos != null) {
			for(int i=0; i<videos.length;i++){
				vid[i] = videos[i].getNombre();
			}
		}
		return vid;
	}
	
	public DtComentario[] seleccionarVideo(String nombre) throws Exception{
		try {
			Video vid = canal.obtenerVideo(nombre);
			Comentario[] comentarios = vid.getComentarios();
			
			DtComentario[] dtComentarios = new DtComentario[comentarios.length];
			if (comentarios != null) {
					for(int i=0; i< comentarios.length;i++){
						dtComentarios[i] = new DtComentario(
												comentarios[i].getId(),
												comentarios[i].getFechaCreacion(),
												comentarios[i].getTexto(),
												comentarios[i].getUser().getNick()
										);
					}
				return dtComentarios;
			}
		} catch (Exception e) {
			throw e;
		}
		return null;
	}
	
}
