package logica;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

public interface IUsuario  {
	public void ingresarDatosUsuario(String nick,String categoria,String nombre,String apellido,String correo,Date fecha,String img);
	public void ingresarDatosCanal(String nombre,String des,boolean privacidad);
	public void AltaUsuario();
	public String[] listarCategorias();
	public void CrearListas()throws Exception;
	public void Finalizar();
	
	/// CU ALTA USUARIO FIN
	
	//  CU DEJAR DE SEGUIR A UN USUARIO PRINCIPIO
	
	public DtUsuario[] seleccionarUsuario(String nick1);
	public void unfollow(String nick2);
	
	// CU DEJAR DE SEGUIR A UN USUARIO FIN
	
	// CU SEGUIR A UN USUARIO INICIO
	
	public DtUsuario seleccionarUsuario2(String nick1);
	public void seguir(String nick2);
 	
	// CU SEGUIR A UN USUARIO FIN
	
	// CU CONSULTA USUARIO INICIO

	public DtUsuario[] listarUsuarios();
	public DtCanal seleccionarCanal();
	public DtListaReproduccion[] listarListasDeReproduccion();
	public DtVideo[] listarVideos();
	public DtUsuario[] listarSeguidores();
	public DtUsuario[] listarMisFollow();
	public DtListaReproduccion seleccionarLista(String nombrelista);
	public DtVideo seleccionarVideo(String nombreVideo)throws Exception;
	
	// CU CONSULTA USUARIO FIN
	
	//MODIFICAR DATOS DE USUARIO PRINCIPIO
	
	public String[] darListaUsuarios();
	public void modificarUsuario(DtUsuario datos);
	
	//MODIFICAR DATOS DE USUARIO FIN
	
	//LISTAR USUARIOS EXISTENTES INICIO
	
	 // ESTE CASO DE USO SERIA UNA SOLA FUNCION QUE ES EXACTAMENTE darListasUsuarios del CU ANTERIOR
	
	//LISTAR USUARIOS EXISTENTES FIN
}	



