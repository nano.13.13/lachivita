package logica;

import java.util.Date;

public interface IVideo {

	public void altaVideo();
	public void seleccionarComentario(int id) throws Exception;
	public void comentarVideo(String nick, Date fecha, String texto, int id);
	public DtUsuario[] listarUsuarios();
	public String[] darListaVideos(String nick);
	public DtComentario[] seleccionarVideo(String nombre) throws Exception;
}
