package logica;

import java.util.Collection;
import java.util.Map;

public abstract class ListaReproduccion {
	
	private String nombre;
//	private String[] categorias;
	private Boolean privado;
	private Map<String, Video> videos;
	
	
	public ListaReproduccion(String nombre, Boolean privado) {
		this.nombre = nombre;
		this.privado = privado;
	}
	 
	public String getNombre() {
		return nombre;
	}
	

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String[] getCategorias() {
		
		if (videos.isEmpty()){
			return null;
		}else{
			Collection<Video> usrs = videos.values();
			Object[] o = usrs.toArray();
			String[] arrCategorias = new String[o.length];
        
			for(int i=0; i<o.length;i++){
				arrCategorias[i]=((Video)o[i]).getCategoria();
			}
			return arrCategorias;
		}
		
		
	}
	
	public Boolean isPrivado() {
		return privado;
	}
	
	public void setPrivado(Boolean privado) throws Exception {
		this.privado = privado;
	}
	

	public Video[] getVideos() {
		if (videos.isEmpty()){
			return null;
		}else{
			Collection<Video> usrs = videos.values();
			Object[] o = usrs.toArray();
			Video[] arrVideos = new Video[o.length];
        
			for(int i=0; i<o.length;i++){
				arrVideos[i]=(Video)o[i];
			}
			return arrVideos;
		}
	}

	public void setVideos(Map<String, Video> videos) {
		this.videos = videos;
	}

	public void agregarVideo(Video v) throws Exception{
	
		if (videos.containsKey(v.getNombre())){
			throw new Exception("Ya existe un Video con el nombre = " + v.getNombre());
		}else{
			videos.put(v.getNombre(), v);
		}
	}

	public Video obtenerVideo(String nombre) throws Exception{
		if (videos.containsKey(nombre)){
			return videos.get(nombre);
		}else{
			throw new Exception("No existe video con nombre = " + nombre);
		}
	}
	
	public void eliminarVideo(Video v){
		
			this.videos.remove(v.getNombre(), v);
	}
	
	
	public DtListaReproduccion getDtLista(){
		
		DtListaReproduccion res = new DtListaReproduccion();
		res.setNombre(nombre);
		res.setPrivado(privado);
		return res;
	}
	

}

class Defecto extends ListaReproduccion{

	public Defecto(String nombre) {
			super(nombre, true);
		
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void setPrivado(Boolean privado) throws Exception {
		if (!privado){
			throw new Exception("Las listas por defecto siempre son privadas.");
		}else{
			super.setPrivado(privado);
		}
	}
	
	
}

class Particular extends ListaReproduccion{

	public Particular(String nombre, Boolean privado) {
		super(nombre, privado);
		// TODO Auto-generated constructor stub
	}
	
}
