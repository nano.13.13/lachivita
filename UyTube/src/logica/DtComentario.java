package logica;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class DtComentario {
	
	private Map<Integer, DtComentario> respuestas;
	private int id;
	private Date fechaCreacion; 
	private String texto;
	private String nick;
	
	public DtComentario(int id, Date fechaCreacion, String texto, String nick) {
		this.setId(id);
		this.setFechaCreacion(fechaCreacion);
		this.setTexto(texto);
		this.setNick(nick);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getNick() {
		return nick;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getTexto() {
		return texto;
	} 

	public void setTexto(String texto) {
		this.texto = texto;
	}

	
	public DtComentario obtenerRespuesta(int id){
		
		if(respuestas.containsKey(id)){
			return respuestas.get(id);
		}else{
			DtComentario result=null;
			Collection<DtComentario> c = respuestas.values();
			Object[] o = c.toArray();
			int i=0;
			while(i<o.length && result==null){
				result=((DtComentario)o[i]).obtenerRespuesta(id);
				i++;
			}
			return result;
		}
		
	}
	
	
}