package logica;

import java.util.Date;

public class DtVideo {

	private String nombre;
	private String descripcion;
	private int duracion;
	private Date fechaPublicacion;
	private String url;
	private String categoria;
	private boolean privado;
	
	public DtVideo(){}
	
	public DtVideo(String nombre, String descripcion, int duracion, Date fechaPublicacion, String url, 
			String categoria, boolean privado){
		this.nombre=nombre;
		this.setDescripcion(descripcion);
		this.setDuracion(duracion);
		this.setFechaPublicacion(fechaPublicacion);
		this.setUrl(url);
		this.setCategoria(categoria);
		this.setPrivado(privado);
		
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public boolean isPrivado() {
		return privado;
	}

	public void setPrivado(boolean privado) {
		this.privado = privado;
	}
		
}
