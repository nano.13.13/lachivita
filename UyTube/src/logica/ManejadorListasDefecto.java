package logica;
import java.util.ArrayList;

public class ManejadorListasDefecto {
	private ArrayList<String> defecto;
	private static ManejadorListasDefecto instance = null;
	
	private ManejadorListasDefecto(){
		this.defecto=new ArrayList<>();
	}
	public static ManejadorListasDefecto getInstance(){
		if(instance == null){
			instance = new ManejadorListasDefecto();
		}
	return instance;	
	}
	public void aniadir(String lista){
		this.defecto.add(lista);
	}
	
	public boolean esvacia(){
		return this.defecto.isEmpty();	
	}
	public String[] getListasDefecto(){
		if (defecto.isEmpty()){
			return null;
		}else{
			Object[] objCat = defecto.toArray();
			String[] arrCat = new String[objCat.length];
			for(int i=0; i<objCat.length; i++){
				arrCat[i]=(String)objCat[i];
			}
			return arrCat;
		}
		
	}
}
