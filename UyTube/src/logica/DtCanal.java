package logica;

public class DtCanal {
	
	private String nombre;
	private String descripcion;
	private String categoria;
	private boolean privado;
	
	
	public DtCanal(String nombre, String descripcion, String categoria, boolean privado){
		this.setNombre(nombre);
		this.setDescripcion(descripcion);
		this.setCategoria(categoria);
		this.setPrivado(privado);
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public boolean isPrivado() {
		return privado;
	}

	public void setPrivado(boolean privado) {
		this.privado = privado;
	}
}
