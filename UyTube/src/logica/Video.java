package logica;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class Video {
	private String nombre;
	private String descripcion;
	private int duracion;
	private Date fechaPublicacion;
	private String url;
	private String categoria;
	private boolean privado;
	private Map<Integer, Comentario> comentarios;
	
	public Video(String nombre, String descripcion, int duracion, Date fechaPublicacion, String url, 
			String categoria, boolean privado){
		this.nombre=nombre;
		this.setDescripcion(descripcion);
		this.setDuracion(duracion);
		this.setFechaPublicacion(fechaPublicacion);
		this.setUrl(url);
		this.setCategoria(categoria);
		this.setPrivado(privado);
		this.setComentarios(new TreeMap<Integer,Comentario>());
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public boolean isPrivado() {
		return privado;
	}

	public void setPrivado(boolean privado) {
		this.privado = privado;
	}

	public Comentario[] getComentarios() {
		if (comentarios.isEmpty()){
			return null;
		}else{
			Collection<Comentario> usrs = comentarios.values();
			Object[] o = usrs.toArray();
			Comentario[] arrComentarios = new Comentario[o.length];
        
			for(int i=0; i<o.length;i++){
				arrComentarios[i]=(Comentario)o[i];
			}
			return arrComentarios;
		}
	}


	public void setComentarios(Map<Integer, Comentario> comentarios) {
		this.comentarios = comentarios;
	}
	
	
	public void agregarComentario(Comentario comentario) {
		comentarios.put(comentario.getId(), comentario);
	}
	
	public Comentario obtenerComentario(int id) throws Exception{
		if(comentarios.containsKey(id)){
			return comentarios.get(id);
		}else{
			Comentario result=null;
			Collection<Comentario> c = comentarios.values();
			Object[] o = c.toArray();
			int i=0;
			while(i<o.length && result==null){
				result=((Comentario)o[i]).obtenerRespuesta(id);
				i++;
			}
			if (result!=null){
				return result;
			}else{
				throw new Exception("No existe Comentario con id=" + id + " asociado al video con nombre=" + this.nombre);
			}
		}
	}
	
	////////// HAMI
	
	public DtVideo getDtVideo(){
		
		DtVideo res = new DtVideo();
		res.setNombre(this.getNombre());
		res.setDescripcion(this.getDescripcion());
		res.setDuracion(this.getDuracion());
		res.setFechaPublicacion(this.getFechaPublicacion());
		res.setUrl(this.getUrl());
		res.setCategoria(this.getCategoria());
		res.setPrivado(this.isPrivado());
		
		return res;
	}
	
}
